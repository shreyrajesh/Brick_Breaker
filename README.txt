GroupNo : 28
Student-1: (Shrey Rajesh, 140050018)
Student-2: (A Hemanth Kumar, 140050036)
Student-3: (Sahith Thallapally, 140050063)

Honor Code:
		I pledge on my honor that I have not given or received any unauthorized assistance on this assignment or any previous task.

Signed by:
		Shrey Rajesh, A Hemanth Kumar, Sahith Thallapally

Contributions:
		Student-1: 100%
		Student-2: 100%
		Student-3: 100%

Comments:
We are taking 1 lateday.

While submitting our updated submission we realised that we had forgotten to submit the dominos.cpp file. We did not make any changes in dominos.cpp after the last commit (it can be seen from our git repository). So kindly grade our dominos.cpp file in time. In the updated version only gprof is added in the report and minor change in makefile (it can be seen from our last commit in the git repository).  

Link to our online repository :
	https://gitlab.com/shreyrajesh/Brick_Breaker.git
