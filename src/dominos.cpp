/*
* Copyright (c) 2006-2009 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

/* 
 * Base code for CS 251 Software Systems Lab 
 * Department of Computer Science and Engineering, IIT Bombay
 * 
 */


#include "cs251_base.hpp"
#include "render.hpp"

#ifdef __APPLE__
  #include <GLUT/glut.h>
#else
  #include "GL/freeglut.h"
#endif

#include <cstring>
using namespace std;

#include "dominos.hpp"



namespace cs251
{
  /**  The is the constructor 
   * This is the documentation block for the constructor.
   */ 
  
  dominos_t::dominos_t()
  {
    //Grid
    /*!
     * \brief <B> b1 : pointer to the game play area </B>
     *
     * This element consists of 4 edges each acting as a limit for play arena 
     */ 
    b2Body* b1;  
    {
      
      b2EdgeShape bottom; 
      bottom.Set(b2Vec2(-40.0f, -3.0f), b2Vec2(40.0f, -3.0f));
      b2BodyDef botbd; 
      b2FixtureDef Bottomfd;
      Bottomfd.restitution = 0.0f;
      Bottomfd.friction = 0.0f;
      Bottomfd.shape = &bottom;
      b1 = m_world->CreateBody(&botbd); 
      b1->CreateFixture(&Bottomfd);
      b1->SetUserData((void*)1);

      b2EdgeShape top; 
      top.Set(b2Vec2(-40.0f, 40.0f), b2Vec2(40.0f, 40.0f));
      b2BodyDef topbd; 
      b2FixtureDef topfd;
      topfd.restitution = 0.0f;
      topfd.friction = 0.0f;
      topfd.shape = &top;
      b1 = m_world->CreateBody(&topbd); 
      b1->CreateFixture(&topfd);

      b2EdgeShape right; 
      right.Set(b2Vec2(40.0f, -3.0f), b2Vec2(40.0f, 40.0f));
      b2BodyDef rtbd; 
      b2FixtureDef rtfd;
      rtfd.restitution = 0.0f;
      rtfd.friction = 0.0f;
      rtfd.shape = &right;
      b1 = m_world->CreateBody(&rtbd); 
      b1->CreateFixture(&rtfd);

      b2EdgeShape left; 
      left.Set(b2Vec2(-40.0f, -3.0f), b2Vec2(-40.0f, 40.0f));
      b2BodyDef ltbd; 
      b2FixtureDef leftfd;
      leftfd.restitution = 0.0f;
      leftfd.friction = 0.0f;
      leftfd.shape = &left;
      b1 = m_world->CreateBody(&ltbd); 
      b1->CreateFixture(&leftfd);
    }

    //Boxes
    /*! \brief <B> 20 Bricks in two rows </B>
    *
    *Each brick is a block with width 2.5 and height 2.0 <br>
    *Each brick is defined seperately using a nested for loop, by giving the co-ordinates of their origin in increasing order <br>
    *Each brick has its own UserData which defines whether it is a power-giving block or not <br>
    *UserData "69" means that the block doesn't contain any power-up <br>
    *UserData "70" means that the block contains a power-up, which decreases the ball's velocity <br>
    *UserData "170" means that the block contains a power-up, which increases the width of the base <br>
    */
    {
    b2PolygonShape shape;
    shape.SetAsBox(2.5f, 2.0f);

    b2FixtureDef fd;
    fd.shape = &shape;
    fd.density = 50.0f;
    fd.restitution = 1.0f;
    fd.friction = 0.0f;
    for (int j = 0; j < 2; ++j)
    {
      for (int i = 0; i < 10; ++i)
      {
      b2BodyDef bd;
      bd.type=b2_staticBody;
      bd.position.Set(-22.8f + 5.0f * i, 31.25f - 4.0f * j);
      b2Body* body = m_world->CreateBody(&bd);
      body->CreateFixture(&fd);
      if(i==1 && j==1)
      	body->SetUserData((void*)70);
      else if(j==1 && i==8)
      	body->SetUserData((void*)170);
      else
      	body->SetUserData((void*)69);
      }
    }

    }
      
    //vertical bar
    /*! \brief <B> Oscillating Vertical Bar </B>
    *
    *This bar is created using b2polygonshape type and defining the breadth and height to be 0.4 and 3.5 respectively <br>
    *Density is set to a very high value so that its position doesn't change after collision with the ball <br>
	*Given a linear velocity of 10 in the positive y-direction initially <br>
	*"mybat" is a global variable which points to this bar <br> 
    */
    {
        b2PolygonShape shape;
        shape.SetAsBox(0.4f, 3.5f);
        b2FixtureDef vertbar;
        vertbar.shape = &shape;
        vertbar.density = 10000000.0f;
        vertbar.restitution = 1.5f;
        vertbar.friction = 0.0f;
        b2BodyDef bd;
        bd.position.Set(-35.0f, 5.0f);
        bd.type = b2_dynamicBody;
        b2Body* Vertbar = m_world->CreateBody(&bd);
        mybat=Vertbar;
        Vertbar->CreateFixture(&vertbar);
        Vertbar->SetLinearVelocity(b2Vec2(0.0,10.0));
    }

    //rotating bar
	/*! \brief <B> Rotating Bar </B>
	*
	*This bar is created using b2polygonshape type and defining the breadth and height to be 0.6 and 3.5 respectively <br>
	*Defined to be of type "kinematicBody" since its position is fixed in the simulation <br>
	*Given an angular velocity of 5 in the anti-clockwise direction initially <br>
	*/
    {
        b2PolygonShape shape;
        shape.SetAsBox(0.6f, 3.5f, b2Vec2(0.f,0.f), 45);

        b2BodyDef bd;
        b2FixtureDef rotbar;
        rotbar.shape = &shape;
        rotbar.density = 10.0f;
        rotbar.friction = 0.0f;
        rotbar.restitution = 0.6f;
        bd.position.Set(-35.0f, 35.0f);
        bd.type = b2_kinematicBody;
        b2Body* rotBar = m_world->CreateBody(&bd);
        rotBar->CreateFixture(&rotbar);
        rotBar->SetAngularVelocity(5.0f);
    }

    //The star
    /*! \brief <B> Triangle1 & Triangle2 : Pointers to two triangles which together make a Rotating Star </B>
    *
    *This star is created using two Triangles with same center facing in the opposite direction <br>
    *Each Triangle is defined with co-ordinates (-3,0), (3,0) & (0,5.2) and center (0,13) <br>
    *Defined to be of type "kinematicBody" since its position is fixed in the simulation <br>
	*Given an angular velocity of 2 in the anti-clockwise direction initially <br>
    */
   	{
	    b2Body* Triangle1;
	    b2PolygonShape star;
	    b2Vec2 vertices1[3];
	    vertices1[0].Set(-3,0);
	    vertices1[1].Set(3,0);
	    vertices1[2].Set(0,5.2);
	    star.Set(vertices1, 3);
	    b2FixtureDef starfd;
	    starfd.shape = &star;
	    starfd.density = 10.0f;
	    starfd.friction = 0.0f;
	    starfd.restitution = 0.0f;
	    b2BodyDef starbd;
	    starbd.position.Set(0.0f, 13.0f);
	    starbd.type = b2_kinematicBody;
	    Triangle1 = m_world->CreateBody(&starbd);
	    Triangle1->CreateFixture(&starfd);
	    Triangle1->SetAngularVelocity(2.0f);
	 
	    b2Body* Triangle2;
	    b2PolygonShape triangle;
	    b2Vec2 vertices2[3];
	    vertices2[0].Set(-3,3.5);
	    vertices2[1].Set(3,3.5);
	    vertices2[2].Set(0,-1.7);
	    triangle.Set(vertices2, 3);
	    b2FixtureDef trifd;
	    trifd.shape = &triangle;
	    trifd.density = 10.0f;
	    trifd.friction = 0.0f;
	    trifd.restitution = 0.0f;
	    b2BodyDef tribd;
	    tribd.position.Set(0.0f, 13.0f);
	    tribd.type = b2_kinematicBody;
	    Triangle2 = m_world->CreateBody(&tribd);
	    Triangle2->CreateFixture(&trifd);
	    Triangle2->SetAngularVelocity(2.0f);
	}
      
    //The ball
    /*! \brief <B> spherebody : pointer to the moving ball </B>
    *
    *This ball is created using b2Circleshape type and defining the radius to be 0.9 <br>
    *This has a UserData of "13" which defines it to be a ball <br>
	*Given a linear velocity of 10 in the negative y-direction initially <br>
	*"myball" is a global variable which points to this ball <br> 
    */

    {
        b2Body* spherebody;
        b2CircleShape circle;
        circle.m_radius = 0.9;

        b2FixtureDef ballfd;
        ballfd.shape = &circle;
        ballfd.density = 1.0f;
        ballfd.friction = 0.1f;
        ballfd.restitution = 1.0f;

        b2BodyDef ballbd;
        ballbd.type = b2_dynamicBody;

        ballbd.position.Set(-5.6f , 23.6f);
        spherebody = m_world->CreateBody(&ballbd);
        myball= spherebody;
        spherebody->CreateFixture(&ballfd);
        spherebody->SetLinearVelocity( b2Vec2(0.0f, -10.0f));
        spherebody->SetUserData((void*)13);
    }

    //Blackhole
	/*! \brief <B> blackbody : pointer to the moving blackhole </B>
	*
	*This hole is created using b2Circleshape type and defining the radius to be 1.5 <br>
	*This has a UserData of "23" which defines it to be a hole <br>
	*Given a linear velocity of 10 in the positive y-direction initially <br>
    *Defined to be of type "kinematicBody" since its position is fixed in the simulation <br>
	*"myhole" is a global variable which points to this hole <br> 
	*/
    {
        b2Body* blackbody;
        b2CircleShape circle;
        circle.m_radius = 1.5;

        b2FixtureDef blackballfd;
        blackballfd.shape = &circle;
        blackballfd.density = 10.0f;
        blackballfd.friction = 0.0f;
        blackballfd.restitution = 0.0f;

        b2BodyDef blackball;
        blackball.type = b2_kinematicBody;

        blackball.position.Set(34.5f , 8.6f);
        blackbody = m_world->CreateBody(&blackball);
        myhole=blackbody;
        blackbody->CreateFixture(&blackballfd);
        blackbody->SetLinearVelocity( b2Vec2(0.0f, 10.0f));
        blackbody->SetUserData((void*)23);
    }

    //The Base
	/*! \brief <B> Moving base </B>
	*
	*This bar is created using b2polygonshape type and defining the breadth and height to be 0.6 and 3.5 respectively <br>
	*This has a UserData of "10" which defines it to be the base <br>
    *Density is set to a very high value so that it doesn't go out of simualtion area after collision with the wall<br>
	*"mybase" is a global variable which points to this base <br> 
	*/
    {
        b2PolygonShape shape;
        shape.SetAsBox(7.0f, 0.8f);
        b2BodyDef bd2;
        bd2.position.Set(0.0f, -1.0f);
        bd2.type = b2_dynamicBody;
        bd2.awake=true;
        b2Body* base = m_world->CreateBody(&bd2);
        mybase = base;
        b2FixtureDef basefd;
        basefd.density = 1000000000000.f;
        basefd.shape = &shape;
        basefd.restitution = 0.0f;
        basefd.friction = 1.0f;
        base->CreateFixture(&basefd);
        base->SetUserData((void*)10);
    }
  }

  sim_t *sim = new sim_t("Brick Breaker", dominos_t::create);
}
