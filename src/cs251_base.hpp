/*
* Copyright (c) 2006-2009 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

/* 
 * Base code for CS 251 Software Systems Lab 
 * Department of Computer Science and Engineering, IIT Bombay
 * 
 */


#ifndef _CS251BASE_HPP_
#define _CS251BASE_HPP_

#include "render.hpp"
#include <Box2D/Box2D.h>
#include <cstdlib>
#include <iostream>
#include <vector>

#define	RAND_LIMIT 32767

namespace cs251
{

  //! What is the difference between a class and a struct in C++?
  class base_sim_t;
  struct settings_t;
  
  //! Why do we use a typedef
  typedef base_sim_t* sim_create_fcn(); 

  //! Simulation settings. Some can be controlled in the GUI.
  struct settings_t
  {
    //! Notice the initialization of the class members in the constructor
    //! How is this happening?
    settings_t() :
      view_center(0.0f, 20.0f),
      hz(60.0f),
      velocity_iterations(8),
      position_iterations(3),
      draw_shapes(1),
      draw_joints(1),
      draw_AABBs(0),
      draw_pairs(0),
      draw_contact_points(0),
      draw_contact_normals(0),
      draw_contact_forces(0),
      draw_friction_forces(0),
      draw_COMs(0),
      draw_stats(0),
      draw_profile(0),
      enable_warm_starting(1),
      enable_continuous(1),
      enable_sub_stepping(0),
      pause(0),
      single_step(0)
    {}
    
    b2Vec2 view_center;
    float32 hz;
    int32 velocity_iterations;
    int32 position_iterations;
    int32 draw_shapes;
    int32 draw_joints;
    int32 draw_AABBs;
    int32 draw_pairs;
    int32 draw_contact_points;
    int32 draw_contact_normals;
    int32 draw_contact_forces;
    int32 draw_friction_forces;
    int32 draw_COMs;
    int32 draw_stats;
    int32 draw_profile;
    int32 enable_warm_starting;
    int32 enable_continuous;
    int32 enable_sub_stepping;
    int32 pause;
    int32 single_step;
  };
  
  struct sim_t
  {
    const char *name;
    sim_create_fcn *create_fcn;

    sim_t(const char *_name, sim_create_fcn *_create_fcn): 
      name(_name), create_fcn(_create_fcn) {;}
  };
  
  extern sim_t *sim;
  
  
  const int32 k_max_contact_points = 2048;  
  struct contact_point_t
  {
    b2Fixture* fixtureA;
    b2Fixture* fixtureB;
    b2Vec2 normal;
    b2Vec2 position;
    b2PointState state;
  };
  
  class base_sim_t : public b2ContactListener
  {
  public:
    
    base_sim_t();

    virtual ~base_sim_t();
    
    void set_text_line(int32 line) { m_text_line = line; }
    void draw_title(int x, int y, const char *string);
    
    virtual void step(settings_t* settings);
    b2Body* mybase;
    b2Body* myball;
    b2Body* myhole;
    b2Body* mybat;
    b2Vec2 pos_2;
    b2Vec2 vel_2;
    bool power_taken;
    bool power_used;

    virtual void keyboard_up(unsigned char key)
    {
        B2_NOT_USED(key); 
        switch (key)
        {
            case 27:
              exit(0);
              break;
            
            case 'd':
                for(int i=0;i<1;i++)
                {
                    mybase->SetLinearVelocity( b2Vec2(0.0f, 0.0f));                    
                    myball->SetAngularVelocity(0.0f);
                }
                break;

            case 'a':
                for(int i=0;i<1;i++)
                {
                    mybase->SetLinearVelocity( b2Vec2(0.0f, 0.0f));
                    myball->SetAngularVelocity( 0.0f);
                }
                break;
            default:
                break;
        }
    }

    virtual void keyboard(unsigned char key)
    {
        B2_NOT_USED(key);
        switch (key)
        {
            case 27:
              exit(0);
              break;
            
            case 'd':
                for(int i=0;i<1;i++)
                {

                    b2Vec2 pos = mybase->GetPosition();
                    if (pos.x < 32)
                    {
                        mybase->SetLinearVelocity( b2Vec2(25.0, 0.0f));
                        myball->SetAngularVelocity( 0.0f);
                    }
                    
                }

                break;
            case 'a':
                for(int i=0;i<1;i++)
                {
                    b2Vec2 pos = mybase->GetPosition();
                    if (pos.x > -32)
                    {
                        mybase->SetLinearVelocity( b2Vec2(-25.0f, 0.0f));
                        myball->SetAngularVelocity( 0.0f);
                    }
                }
                break;
            case 'q':
                if(power_taken && !power_used)
                {
                  b2Vec2 vel;
                  vel=myball->GetLinearVelocity();
                  myball->SetLinearVelocity(0.5*vel);
                  power_used=true;
                }
                break;
              //! The default case. Why is this needed?
            default:
                break;
            
        }
    }

    void shift_mouse_down(const b2Vec2& p) { B2_NOT_USED(p); }
    virtual void mouse_down(const b2Vec2& p) { B2_NOT_USED(p); }
    virtual void mouse_up(const b2Vec2& p) { B2_NOT_USED(p); }
    void mouse_move(const b2Vec2& p) { B2_NOT_USED(p); }

    
    // Let derived tests know that a joint was destroyed.
    virtual void joint_destroyed(b2Joint* joint) { B2_NOT_USED(joint); }
    // Callbacks for derived classes.
    /*! \brief <B> body1 : pointer to the first body involved in collision </B> <br>
    * <B> body2 : pointer to the second body involved in collision <br> 
    *data1 stores the UserData of body1 <br>
    *data2 stores the UserData of body2 <br>
    */
    virtual void BeginContact(b2Contact* contact) 
    { 
      B2_NOT_USED(contact); 
      b2Body* body1 = contact->GetFixtureA()->GetBody();
      b2Body* body2 = contact->GetFixtureB()->GetBody();
      int data1=(long) body1->GetUserData();
      int data2=(long) body2->GetUserData();
      pos_2=myball->GetPosition();
      vel_2 = myball->GetLinearVelocity();
      int count=0;
      //69 is for bricks
      if(data1==69)
      {
        //666 means to delete the brick
        body1->SetUserData((void*)666);
        count++;
      }
      else if(data2==69)
      {
        body2->SetUserData((void*)666);
        count++;
      }
      //70 is for the special power-containing block
      else if(data1==70)
      {
        //667 means to delete the brick and drop a power-down
        body1->SetUserData((void*)667);
      }
      else if(data2==70)
      {
        body2->SetUserData((void*)667);
      }
      //170 is for special power-containing block
      else if(data1==170)
      {
        body1->SetUserData((void*)1667);
      }
      else if(data2==170)
      {
        body2->SetUserData((void*)1667);
      }
      //13 is for moving ball
      //23 is for blackhole
      else if(data1==13 && data2==23)//blackhole and ball
      {
        //696 means to delete ball and create a new ball above the star
        body1->SetUserData((void*)696);
      }
      else if(data1==23 && data1==13)//blackhole and ball
      {
        body2->SetUserData((void*)696);
      }


      //100 is for power downs that have been created after the ball hits a particular brick
      //100 & 10 means that this is the collision between base and power-down
      else if(data1==100 && data2==10)
      {
        //1000 means that user has taken the power-down
        body1->SetUserData((void*)1000);
      }
      else if(data2==100 && data1==10)
      {
        body2->SetUserData((void*)1000);
      }
      //200 is for power downs that have been created after the ball hits a particular brick
      else if(data1==200 && data2==10)
      {
        body1->SetUserData((void*)2000);
      }
      else if(data2==200 && data1==10)
      {
        body2->SetUserData((void*)2000);
      }
      //1 is for the base of the entire simulation
      //100 & 1 means that this is the collision between power-down and base of the simulation
      else if(data2==100 && data1==1)
      {
        //1500 means that the user hasn't taken the powerup
        body2->SetUserData((void*)1500);
      }
      else if(data1==100 && data2==1)
      {
        body1->SetUserData((void*)1500);
      }
      //1 is for the base of the entire simulation
      //2500 means that the user hasn't taken the powerup
      else if(data2==200 && data1==1)
      {
        body2->SetUserData((void*)2500);
      }
      else if(data1==200 && data2==1)
      {
        body1->SetUserData((void*)2500);
      }
      else if(data1==100 && data2==13)
      {
        //1100 means that we need to redefine the powerdown to bypass the collision between ball and power
        body1->SetUserData((void*)1100);
      }
      else if(data1==13 && data2==100)
      {
        body2->SetUserData((void*)1100);
      }
      else if(data1==200 && data2==13)
      {
        body1->SetUserData((void*)2100);
      }
      else if(data1==13 && data2==200)
      {
        body2->SetUserData((void*)2100);
      }
      else if((data1==13 && data2==1) ||(data1==1 && data2==13))//game over(not completed)
      {
        b2Body* object = m_world->GetBodyList();
        while (object)
        {
          object->SetUserData((void*)666);
          object=object->GetNext();
        }
      }
    }
    virtual void end_contact(b2Contact* contact) { B2_NOT_USED(contact); }
    virtual void pre_solve(b2Contact* contact, const b2Manifold* oldManifold);
    virtual void post_solve(const b2Contact* contact, const b2ContactImpulse* impulse)
    {
      B2_NOT_USED(contact);
      B2_NOT_USED(impulse);
    }
    void delete_block()
    {
      b2Body* object = m_world->GetBodyList();
      while (object)
      {
          b2Body* name = object;
          object = object->GetNext();
          int value = (long)name->GetUserData();
          if (value == 666)
          {
            m_world->DestroyBody(name);
          }
          else if(value==696)
          {
            m_world->DestroyBody(name);
            b2Body* spherebody;
            b2CircleShape circle;
            circle.m_radius = 0.9;
            b2FixtureDef ballfd;
            ballfd.shape = &circle;
            ballfd.density = 1.0f;
            ballfd.friction = 0.1f;
            ballfd.restitution = 1.0f;
            b2BodyDef ballbd;
            ballbd.type = b2_dynamicBody;
            ballbd.position.Set(-5.6f , 23.6f);
            spherebody = m_world->CreateBody(&ballbd);
            myball= spherebody;
            spherebody->CreateFixture(&ballfd);
            spherebody->SetLinearVelocity( b2Vec2(0.0f, -12.5f));
            spherebody->SetUserData((void*)13);
          }

          else if(value==667)
          {
            m_world->DestroyBody(name);
            b2PolygonShape shape;
            shape.SetAsBox(1.0f, 1.0f, b2Vec2(0.0f,0.0f), 0);// breadth, height, position, angle

            b2BodyDef bd;
            b2FixtureDef rotbar;
            rotbar.shape = &shape;
            rotbar.density = 10.0f;
            rotbar.friction = 0.0f;
            rotbar.restitution = 0.6f;
            bd.position.Set(-17.2f,27.25f);
            bd.type = b2_dynamicBody;
            b2Body* rotBar = m_world->CreateBody(&bd);
            rotBar->CreateFixture(&rotbar);
            rotBar->SetUserData((void*)100);
            rotBar->SetLinearVelocity(b2Vec2(0.0f, -10.0f));
          }
          else if(value==1667)
          {
            m_world->DestroyBody(name);

            b2Body* Triangle1;
            b2PolygonShape star;
            b2Vec2 vertices1[3];
            vertices1[0].Set(-1,0);
            vertices1[1].Set(1,0);
            vertices1[2].Set(0,1.732);
            star.Set(vertices1, 3);
            b2FixtureDef starfd;
            starfd.shape = &star;
            starfd.density = 10.0f;
            starfd.friction = 0.0f;
            starfd.restitution = 0.0f;
            b2BodyDef starbd;
            starbd.position.Set(17.2f,27.25f);
            starbd.type = b2_dynamicBody;
            Triangle1 = m_world->CreateBody(&starbd);
            Triangle1->CreateFixture(&starfd);
            Triangle1->SetUserData((void*)200);
            Triangle1->SetLinearVelocity(b2Vec2(0.0f, -10.0f));
          }

          else if(value==1000)
          {
            m_world->DestroyBody(name);
            power_taken=true;
          }
          else if(value==2000)
          {
            m_world->DestroyBody(name);
            b2Vec2 pos;
            b2Vec2 vel;
            pos=mybase->GetPosition();
            vel = mybase->GetLinearVelocity();
            m_world->DestroyBody(mybase);
            b2PolygonShape shape;
            shape.SetAsBox(9.0f, 0.8f);
            b2BodyDef bd2;
            bd2.position.Set(pos.x,pos.y);
            bd2.type = b2_dynamicBody;
            bd2.awake=true;
            b2Body* base = m_world->CreateBody(&bd2);
            mybase = base;
            b2FixtureDef basefd;
            basefd.density = 1000000000000.f;
            basefd.shape = &shape;
            basefd.restitution = 0.0f;
            basefd.friction = 1.0f;
            base->CreateFixture(&basefd);
            base->SetUserData((void*)10);
          }
          else if(value==1100)
          {
            b2Vec2 pos;
            pos=name->GetPosition();
            m_world->DestroyBody(name);
            b2PolygonShape shape;
            shape.SetAsBox(1.0f, 1.0f, b2Vec2(0.0f,0.0f), 0);// breadth, height, position, angle
            b2BodyDef bd;
            b2FixtureDef rotbar;
            rotbar.shape = &shape;
            rotbar.density = 10.0f;
            rotbar.friction = 0.0f;
            rotbar.restitution = 0.6f;
            bd.position.Set(pos.x,pos.y);
            bd.type = b2_dynamicBody;
            b2Body* rotBar = m_world->CreateBody(&bd);
            rotBar->CreateFixture(&rotbar);
            rotBar->SetUserData((void*)100);
            rotBar->SetLinearVelocity(b2Vec2(0.0f, -10.0f));
            m_world->DestroyBody(myball);
            b2Body* spherebody;
            b2CircleShape circle;
            circle.m_radius = 0.9;
            b2FixtureDef ballfd;
            ballfd.shape = &circle;
            ballfd.density = 1.0f;
            ballfd.friction = 0.1f;
            ballfd.restitution = 1.0f;
            b2BodyDef ballbd;
            ballbd.type = b2_dynamicBody;
            ballbd.position.Set(pos_2.x,pos_2.y);
            spherebody = m_world->CreateBody(&ballbd);
            myball= spherebody;
            spherebody->CreateFixture(&ballfd);
            spherebody->SetLinearVelocity(b2Vec2(vel_2.x,vel_2.y));
            spherebody->SetUserData((void*)13);
          }
          else if(value==2100)
          {
            b2Vec2 pos;
            pos=name->GetPosition();
            m_world->DestroyBody(name);
            b2Body* Triangle1;
            b2PolygonShape star;
            b2Vec2 vertices1[3];
            vertices1[0].Set(-1,0);
            vertices1[1].Set(1,0);
            vertices1[2].Set(0,1.732);
            star.Set(vertices1, 3);
            b2FixtureDef starfd;
            starfd.shape = &star;
            starfd.density = 10.0f;
            starfd.friction = 0.0f;
            starfd.restitution = 0.0f;
            b2BodyDef starbd;
            starbd.position.Set(pos.x,pos.y);
            starbd.type = b2_dynamicBody;
            Triangle1 = m_world->CreateBody(&starbd);
            Triangle1->CreateFixture(&starfd);
            Triangle1->SetUserData((void*)200);
            Triangle1->SetLinearVelocity(b2Vec2(0.0f, -10.0f));
            m_world->DestroyBody(myball);
            b2Body* spherebody;
            b2CircleShape circle;
            circle.m_radius = 0.9;
            b2FixtureDef ballfd;
            ballfd.shape = &circle;
            ballfd.density = 1.0f;
            ballfd.friction = 0.1f;
            ballfd.restitution = 1.0f;
            b2BodyDef ballbd;
            ballbd.type = b2_dynamicBody;
            ballbd.position.Set(pos_2.x,pos_2.y);
            spherebody = m_world->CreateBody(&ballbd);
            myball= spherebody;
            spherebody->CreateFixture(&ballfd);
            spherebody->SetLinearVelocity(b2Vec2(vel_2.x,vel_2.y));
            spherebody->SetUserData((void*)13);
          }
          else if(value==1500)
          {
            m_world->DestroyBody(name);
          }
          else if(value==2500)
          {
            m_world->DestroyBody(name);
          }
      }
    }

    /*! \brief <B> Adjusts the velocity </B> <br>
    * <B> Responsible for oscillating both vertical bar and blackhole </B><br>
    * Sets the ball's velocity to 35.0 if it is greater than 35.0 <br>
    * Sets the ball's velocity to 15.0 if it is lesser than 15.0 <br>
    */
    void adjust_velocity()
    {
      const b2Vec2 velocity = myball->GetLinearVelocity();
      const float32 speed = velocity.Length();
      const b2Vec2 holepos = myhole->GetPosition();
      const b2Vec2 batpos = mybat->GetPosition();
      if (speed > 35.0)
      {
        myball->SetLinearVelocity((35.0/speed)*velocity);
      }
      else if(speed < 12.5)
      {
        myball->SetLinearVelocity((12.5/speed)*velocity);
      }

      if(holepos.y > 35.0)
        myhole->SetLinearVelocity(b2Vec2(0,-10.0));
      if(holepos.y < 4.0 )
        myhole->SetLinearVelocity(b2Vec2(0,10.0));

      if(batpos.y > 20.0)
        mybat->SetLinearVelocity(b2Vec2(0,-10.0));
      if(batpos.y < 7.0 )
        mybat->SetLinearVelocity(b2Vec2(0,10.0));
    }
    void delete_game()//game completed
    {
      b2Body* object = m_world->GetBodyList();
      int count=0;
      while(object)
      {
        int value = (long)object->GetUserData();
        if(value==69 || value==70 || value==170)count++;
        object = object->GetNext();
      }
      object = m_world->GetBodyList();
      if(count==0)
      {
        while(object)
        {
          b2Body* name = object;
          object = object->GetNext();
          m_world->DestroyBody(name);
        }
      }
    }
  //!How are protected members different from private memebers of a class in C++ ?
  protected:

    //! What are Friend classes?
    friend class contact_listener_t;
    
    b2Body* m_ground_body;
    b2AABB m_world_AABB;
    contact_point_t m_points[k_max_contact_points];
    int32 m_point_count;

    debug_draw_t m_debug_draw;
    int32 m_text_line;
    b2World* m_world;

    int32 m_step_count;
    
    b2Profile m_max_profile;
    b2Profile m_total_profile;
  };
}

#endif
